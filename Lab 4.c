/*
Eric Schultz
Lab 4
eschultz@pacbell.net
*/

#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <stdlib.h>

#define GLOBAL_PROFIT 0.20

//Extra function declarations

int booksToBeOrdered(int, char, char);
double calculateProfit(float, int);
void output_function(char, char, char, char, char, char, char,
                     char, char, char, int, double);


int main (void) //Input function
{

    printf("Eric Schultz\nLab4\neschultz@pacbell.net\n");
    char d1;
    char d2;
    char d3;
    char d4;
    char d5;
    char d6;
    char d7;
    char d8;
    char d9;
    char d10;
    float price;
    char RorS;
    char newOrOld;
    int expectedEnrollment;
    int copiesNeeded;
    double profit;

    //Input ISBN number, price, enrolled student #,
    //whether the book is required or suggested, new or old

    printf("\nEnter book's 10-digit ISBN number: ");
    scanf(" %c %c %c %c %c %c %c %c %c %c",
          &d1, &d2, &d3, &d4, &d5, &d6, &d7, &d8, &d9, &d10);
    printf("\nEnter price per copy: ");
    scanf("%f", &price);
    printf("\nExpected class enrollment: ");
    scanf("%d", &expectedEnrollment);
    printf("\nEnter 'R' if required or 'S' if suggested: ");
    scanf(" %c", &RorS);
    printf("\nEnter 'N' if new text or 'O' if not new text: ");
    scanf(" %c", &newOrOld);

    //Call functions to appear program from main

    copiesNeeded = booksToBeOrdered(expectedEnrollment, newOrOld, RorS);
    profit = calculateProfit(price, copiesNeeded);
    output_function(d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, copiesNeeded, profit);

    return 0;
}


int booksToBeOrdered(int enrollment, char newOrOld, char RorS)
{
    //Pre: expected enrollment, number of copies needed, whether is the book is used
    //or new and whether the book is required or suggested
    //calculate number of copies needed based on whether new or used, suggested or required

    float numberNeeded;

    if (tolower(newOrOld) == 'n' && tolower(RorS) == 'r')
    {
        numberNeeded = enrollment*0.9f;
    }
    else if (tolower(newOrOld) == 'n' && tolower(RorS) == 's')
    {
        numberNeeded = enrollment*0.4f;
    }
    else if (tolower(newOrOld) == 'o' && tolower(RorS) == 'r')
    {
        numberNeeded = enrollment*0.65f;
    }
    else if (tolower(newOrOld) == 'o' && tolower(RorS) == 's')
    {
        numberNeeded = enrollment*0.2f;
    }
    else
    {
        printf("\nInvalid Input\n");
        exit(100);
    }

    //Post: number of copies needed
    return (ceil(numberNeeded));
}


double calculateProfit(float price, int copiesNeeded)
{
    //Pre: amount of price
    //Calculating profit with internal variable
    double profit;

    profit = GLOBAL_PROFIT*(price*copiesNeeded);
    //Post: amount of profit
    return (profit);
}

void output_function(char n1, char n2, char n3, char n4, char n5, char n6,
                     char n7, char n8, char n9, char n10, int copiesNeeded, double profit)
{
    //Pre: 10 seperate ISBN numbers, copies needed, profit
    //Print all output formatted
    printf("\nISBN: %c-%c%c-%c%c%c%c%c%c-%c",
            n1, n2, n3, n4, n5, n6, n7, n8, n9, n10);
    printf("\nCopies Needed: %.f", ceil(copiesNeeded));
    printf("\nProfit: $%8.2lf", profit);
    //Post: (no return)
}
