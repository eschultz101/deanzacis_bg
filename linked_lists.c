#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

typedef struct person{
	char name[10];
	float inches;
	struct person* next;
	}PERSON;

float calcAvg(PERSON* head);
PERSON* getData(PERSON*);
char menu(void);
void print(PERSON*);
PERSON* position(PERSON* head, PERSON* adding);
PERSON* deleteOne(PERSON* head);
PERSON* addOne(PERSON* head);

int main (void)
{
  char ch; FILE* fp;
  PERSON* head = NULL;
  PERSON* walker;
  float avg;

	 /*The current list is entered from the file*/
	 /*The list is in descending order by heights*/
	 head=getData(head);
	 print(head);

	 while ((ch=menu()) != 'E')
	  {
		switch (ch)
		{
		 case 'P':  print(head); break;
		 case 'A':  head = addOne(head);break;
		 case 'D':  head = deleteOne(head); break;
		 case 'V':  printf("\nAverage is: %4.1f", avg = calcAvg(head));break;
         case 'T':  printf("\nWrite function to output the tallest person");break;
		 case 'S':  printf("\nWrite function to output the name of the");
						printf(" shortest person");break;
		 case 'F':  printf("\nWrite function to output the linked list");
						printf(" to a file");break;
		 case 'E':  printf("\nExit the program\n");
						break;
		 default:   printf("\n Invalid.  Enter choice again"); break;
		 }/*switch*/
      }/*while*/
	  /*Output list with additions and deletions to file*/
	  fp=fopen("results.txt","w");
	  walker=head;
	  while(walker)
		  {
			 fprintf(fp,"%s %f\n",walker->name,walker->inches);
			 walker=walker->next;
			 }
    fclose(fp);
	 return 0;

}

char menu(void)
{  /*Pre:  Nothing
    Post:  char representing user's choice*/
			char choice;
			printf("\n\nEnter 'P' to print the entire list");
			printf("\n      'A' to add a person to the list");
			printf("\n      'D' to delete a person from the list");
			printf("\n      'V' to output the average height");
			printf("\n      'T' to print out the tallest person");
			printf("\n      'S' to print the name of the shortest person");
			printf("\n      'F' to output to a file.\n");
			printf("\n      'E' to exit the program.\n");

			scanf(" %c", &choice);
		  return toupper(choice);
}

float calcAvg(PERSON* head)
{
    float avg; float sum = 0; int count = 0;
    PERSON* walker = head;

    while(walker)
    {
        sum+= walker->inches;
        count++;
        walker = walker->next;
    }
    if (count) avg = sum / ((float)count);

    return avg;
}

PERSON* position(PERSON* head, PERSON* add)
{
    PERSON* walker = head;
    PERSON* prev = NULL;

    //Empty list
    if(head == NULL)
        head = add;
    else
    {
        while(walker != NULL && walker->inches < add->inches)
        {
            prev = walker;
            walker = walker->next;
        }
    if(prev != NULL)
    {
        add->next = prev->next;
        prev->next = add;
    }
    else
    {
        add->next = head;
        head = add;
    }

   }

    return head;
}

PERSON* addOne(PERSON* head)
{
    PERSON* adding;
    adding = (PERSON*)malloc(sizeof(PERSON));

    printf("Enter person's name: \n");
    scanf(" %[^\n]", adding->name);
    printf("\nEnter person's height: \n");
    scanf("%f", &adding->inches);

    adding->next = NULL;

    head = position(head, adding);

    return head;
}

PERSON* deleteOne(PERSON* head)
{
    PERSON* walker = head;
    PERSON* prev = NULL;
    char tempName[10];

    if(head == NULL)
    {
        printf("Empty List");
        return head;
    }

    printf("Who do you wish to delete?\n");
    scanf(" %[^\n]", tempName);

    while(walker != NULL && strcmp(walker->name, tempName) != 0)
    {
        prev = walker;
        walker = walker->next;
    }
    if(walker == NULL)
    {
        printf("No such person\n");
    }
    else
        if(prev)
        {
            prev->next = walker->next;
            free(walker);
        }
        else
        {
            head = head->next;
            free(walker);
        }

    return head;
}

PERSON* getData(PERSON* head)
{
    FILE* fpIn;
    char hold[81];
    PERSON* add;

    if(!(fpIn = fopen("linkedListsData.txt", "r")))
    {
        printf("File Not Found\n");
        exit(100);
    }

    while(fgets(hold, sizeof(hold) - 1, fpIn))
    {
        //puts(hold);
        add = (PERSON*)malloc(sizeof(PERSON));
        sscanf(hold, "%[^1234567890] %f", add->name, &add->inches);
        add->next = NULL;
        head = position(head, add);
        printf("%s %.1f\n", add->name, add->inches);
    }

    fclose(fpIn);

    return add;
}

void print(PERSON* head)
{
    PERSON* walker = head;

    printf("\n\n");
    printf("Outputting all info on file\n");
    while(walker != NULL)
    {
        printf("%-30s %4.1f\n", walker->name, walker->inches);
        walker = walker->next;
    }

    return;
}

