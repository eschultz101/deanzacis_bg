#include <stdio.h>

void reverse(int*);

int main(void)
{
    int arr[5] = {1, 2, 3, 4, 5};
    int i, j;

    printf("Eric Schultz\n\n");
    printf("\nArray before reversal: \n\n");
    for(i = 0; i < 5; i++)
    printf("%d ", *(arr + i));

    reverse(arr);

    printf("\n\nArray after reversal: \n\n");
    for(i = 0; i < 5; i++)
    printf("%d ", *(arr + i));

    printf("\n\n");
    return 0;
}

void reverse(int* arr)
{
    int i, j;
    int temp[5];

    for(i = 0; i < 5; i++)
    {
        *(temp + i) = *(arr + i);
    }
    printf("\n");

    for(i = 0; i < 5; i++)
    {
        *(arr + i) = *(temp + (4 - i));
    }

    return;
}


