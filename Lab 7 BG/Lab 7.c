/*
Eric Schultz
Lab 7
eschultz@pacbell.net
*/

/*Structures have tank name, front armor,
side armor, rear armor (in mm) and pointer to next.*/

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

typedef struct tank
{
    char name[10];
    int front;
    int sides;
    int rear;
    struct tank* next;
}TANK;

TANK* getData(TANK*);
char menu(void);
void print(TANK*);
TANK* position(TANK* first, TANK* adding);
TANK* deleteOne(TANK* first);
TANK* addOne(TANK* first);
void filePrint(FILE*, TANK*);
TANK* modify(TANK* first);

int main(void)
{
    FILE* fpOut;
    char ch;
    TANK* first = NULL;
    TANK* walker;

    //Create linked list with info from input file.
    first = getData(first);
    //Print all info for confirmation.
    print(first);

    //Prompt user for command.  Each command has a corresponding function
    while ((ch=menu()) != 'Q')
	  {
		switch (ch)
		{
		 case 'P':  print(first); break;
		 case 'A':  first = addOne(first); break;
		 case 'D':  first = deleteOne(first); break;
		 case 'F':  filePrint(fpOut, first); break;
		 case 'M':  first = modify(first); break;
		 case 'Q':  exit(100); break;
		 //If input is not any of the above commands, displays...
		 default:   printf("\n Invalid.  Enter choice again\n"); break;
		 }
      }

    return;
}

TANK* getData(TANK* first)
{
    //Pre: First tank in linked list (currently pointing to NULL)
    //Post: Linked list of tanks
    FILE* fpIn;
    char hold[81];
    TANK* add;

    //Check for file existence...
    if(!(fpIn = fopen("tanks.txt", "r")))
    {
        printf("File Not Found\n");
        exit(100);
    }

    //Using fgets and a buffer to scan in all info at once
    while(fgets(hold, sizeof(hold) - 1, fpIn))
    {
        add = (TANK*)malloc(sizeof(TANK));
        //Assigning each line to variables
        sscanf(hold, "%s %d %d %d", add->name, &add->front, &add->sides, &add->rear);
        //Setting the next pointer to NULL (until it gets to position function)
        add->next = NULL;
        first = position(first, add);
    }

    fclose(fpIn);

    return add;
}

TANK* position(TANK* first, TANK* add)
{
    //Pre: First tank in linked list, tank being added to list
    //Post: First tank in linked list (with new one in the list wherever)

    TANK* walker = first;
    TANK* prev = NULL;

    //If list is empty, added tank becomes the only structure in list
    if(first == NULL)
        first = add;
    else
    {
        //While walker is not nothing, and it fits in between two other
        //parts of the linked list, set previous one to current and current to next
        while(walker != NULL && walker->name > add->name)
        {
            prev = walker;
            walker = walker->next;
        }
    if(prev != NULL)
    {
        add->next = prev->next;
        prev->next = add;
    }
    else
    {
        add->next = first;
        first = add;
    }
   }

    return first;
}

char menu(void)
{
    //Pre: None
    //Post: Char representing user's choice
			char choice;
			printf("\n\nEnter: 'P' to print the entire list.");
			printf("\n       'A' to add a tank to the list.");
			printf("\n       'D' to delete a tank from the list.");
			printf("\n       'F' to output to a file.\n");
			printf("\n       'M' to modify the armor of a tank.");
			printf("\n       'Q' to exit the program.\n");

			scanf(" %c", &choice);
		  return toupper(choice);
}

void print(TANK* first)
{
    //Pre: First tank in linked list
    //Post: none

    //Set the list walker to the first tank in the linked list.
    TANK* walker = first;

    printf("\n\n");
    printf("Outputting all info on file:\n");
    while(walker != NULL)
    {
        //Outputting all fields of each structure in linked list
        printf("%-10s %d %d %d\n", walker->name, walker->front, walker->sides, walker->rear);
        walker = walker->next;
    }

    return;
}

TANK* addOne(TANK* first)
{
    //Pre: First tank in linked list
    //Post: Pointer to first tank (with new tank in linked list)
    TANK* adding;
    adding = (TANK*)malloc(sizeof(TANK));

    printf("Enter tank's name: \n");
    scanf(" %[^\n]", adding->name);
    printf("\nEnter tank's front armor (in mm): \n");
    scanf("%d", &adding->front);
    printf("\nEnter tank's side armor: \n");
    scanf("%d", &adding->sides);
    printf("\nEnter tank's rear armor: \n");
    scanf("%d", &adding->rear);

    adding->next = NULL;

    first = position(first, adding);

    return first;
}

TANK* deleteOne(TANK* first)
{
    //Pre: First tank in linked list
    //Post: First tank again; but without the deleted  tank
    TANK* walker = first;
    TANK* prev = NULL;
    char tempName[10];

    if(first == NULL)
    {
        printf("List is empty");
        return first;
    }

    printf("Which tank do you wish to delete?\n");
    scanf(" %[^\n]", tempName);

    //If the names are the same, set previous to next and remove the tank structure
    while(walker != NULL && strcmp(walker->name, tempName) != 0)
    {
        prev = walker;
        walker = walker->next;
    }
    if(walker == NULL)
    {
        //If names do not match...
        printf("No such tank\n");
    }
    else
        if(prev)
        {
            prev->next = walker->next;
            free(walker);
        }
        else
        {
            first = first->next;
            free(walker);
        }

    return first;
}

void filePrint(FILE* fpOut, TANK* first)
{
    //Pre: File pointer, first structure of tanks
    //Post: None

    //Making .txt file to use
    fpOut = fopen("lab7output.txt", "w");

    //Setting the walker to the first structure
    TANK* walker = first;

    fprintf(fpOut, "\n\n");
    fprintf(fpOut, "Outputting all info on file:\n");
    while(walker != NULL)
    {
        fprintf(fpOut, "%-10s %d %d %d\n", walker->name, walker->front, walker->sides, walker->rear);
        walker = walker->next;
    }

    //Closing file
    fclose(fpOut);

    return;
}

TANK* modify(TANK* first)
{
    TANK* walker = first;
    TANK* prev = NULL;
    TANK* tempTank;
    char tempName[10];

    //Empty list
    if(walker == NULL)
        printf("List is empty");
    else if(walker)
    {
        printf("Which tank would you like to modify?");
        scanf(" %[^\n]", tempName);
        while(walker != NULL && strcmp(walker->name, tempName) != 0)
        {
            prev = walker;
            walker = walker->next;
        }
        if(walker == NULL)
        {
            printf("No such tank\n");
        }
        else
        {
            tempTank = (TANK*)malloc(sizeof(TANK));
            strcpy(tempTank->name, tempName);
            printf("Enter new front armor for %s:\n", tempName);
            scanf("%d", &tempTank->front);
            printf("Enter new side armor for %s:\n", tempName);
            scanf("%d", &tempTank->sides);
            printf("Enter new rear armor for %s:\n", tempName);
            scanf("%d", &tempTank->rear);

            tempTank->next = NULL;

            //Delete old structure
            if(prev)
                prev->next = walker->next;
            else
                first = walker->next;
            free(walker);
            //place new structure
            first = position(first, tempTank);
        }
    }
typedef struct queueElt {

}

    return first;
}
