/*Eric Schultz
Lab 3
eschultz@pacbell.net*/

#include <stdio.h>

int main (void)
{
    printf("Eric Schultz\nLab 3\neschultz@pacbell.net\n\n");
    float a, b, c;
    float* x = &a;
    float* p = &b;
    float* q = &c;
    float** y = &p;
    float** r = &q;
    float*** z = &r;

    //ask for initial values
    printf("Enter value for 'a': ");
    scanf("%f", &a);
    printf("Enter value for 'b': ");
    scanf("%f", &b);

    //calculate using pointers only
    ***z = (*x) * (**y);

    //print all variables using x, y, z.
    printf("\n%5.2f * %5.2f = %5.2f\n", *x, **y, ***z);

    return 0;
}
