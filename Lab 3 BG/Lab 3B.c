/*Eric Schultz
Lab 3B
eschultz@pacbell.net*/
#include <stdio.h>

float grab(float*, int, int);

int main(void)
{
    float arr[3][3] = { {41.87, 436.3, 33.0},
                        {57.2, 41.9, 126.98},
                        {15.1, 66.7, 13.37}};
    float* pf = arr[0];

    int i; int j; int row; int col; float num;

    printf("Eric Schultz\nLab 3B\neschultz@pacbell.net\n\n");

    for (i = 0; i < 3; i++)
     {
      for (j = 0; j < 3; j++)
        printf("%5.2f\t", arr[i][j]);
      printf("\n");
      }
    //hint
    /*
     printf("\nThe Hint\n");
     for (i = 0; i < 9; i++)
        {
        printf("%f\t", *(pf + i));
        if ( (i +1) % 3 == 0) printf("\n");
        }*/
    //Input index of desired element
      printf("\nEnter a row number between 1 and 3\n");
      scanf("%d", &row);
      printf("Enter a column number between 1 and 3\n");
      scanf("%d", &col);

      num = grab(pf, row-1, col-1);

      if (row > 0 && row < 4 && col > 0 && col < 4)
         printf("\nThe (%d,%d) array element is %5.2f\n\n", row, col, num);
      else
         printf("Out of Bounds\n");

         system("pause");
      return 0;
     }

float grab(float* pf, int row, int col)
{
    float num;
    int i; int j;

  for (i = 0; i < 9; i++)
        {
        printf("%5.2f\t", *(pf + i));
        if ( (i +1) % 3 == 0) printf("\n");
        }

    num = *(pf + (row*3) + col);

    return num;
}

/*Output
Eric Schultz
Lab 3B
eschultz@pacbell.net

41.87   436.30  33.00
57.20   41.90   126.98
15.10   66.70   13.37

Enter a row number between 1 and 3
1
Enter a column number between 1 and 3
1
41.87   436.30  33.00
57.20   41.90   126.98
15.10   66.70   13.37

The (1,1) array element is 41.87

Press any key to continue . . .


Eric Schultz
Lab 3B
eschultz@pacbell.net

41.87   436.30  33.00
57.20   41.90   126.98
15.10   66.70   13.37

Enter a row number between 1 and 3
2
Enter a column number between 1 and 3
3
41.87   436.30  33.00
57.20   41.90   126.98
15.10   66.70   13.37

The (2,3) array element is 126.98

Press any key to continue . . .

*/
