/*
Eric Schultz
Lab 6
eschultz@pacbell.net
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SKATERS 24

typedef struct skater
                    {
                       char name[50];
                       int element[8];
                       float baseValue[8];
                       float totalBase;
                       int scores[8][12];
                       int totalScores[8];
                       float finalScore;
                    }SKATER;

SKATER* getData(char eventName[], int* num);
void sortSelect(SKATER* add, int num);
void output(SKATER* add, int num, char eventName[]);
void compute(SKATER player[], int* num);
int findHigh(SKATER* player, int element);
int findLow(SKATER* player, int element);

int main (void)
{
    char eventName[30];
    SKATER* add;
    int num = 0;

    //Setting event name to null for first pass in getData
    eventName[0] = '\0';
    //Function to get data for each skater
    add = getData(eventName, &num);
    //Computes total of base values and technical score
    compute(add, &num);
    //Sorts skaters by technical score (finalScore)
    sortSelect(add, num);
    //Outputs sorted structures
    output(add, num, eventName);

    return 0;
}

SKATER* getData(char eventName[], int* num)
{
    //Pre: Array of chars for the event name, empty actual number of skaters
    //Post: Filled array of structures
    FILE* fpIn;
    char buffer[100];
    int i = 8;
    int k = 0;
    SKATER* add;

    //Allocating memory for the max number of skaters possible.
    add = (SKATER*)calloc(MAX_SKATERS, sizeof(SKATER));

    //Checking for file existence
    if(!(fpIn = fopen("OlympicMensShort.txt", "r")))
        {
         printf("FILE NOT FOUND\n");
         exit(100);
        }

    //Loop to assign data to correct variables
    while(fgets(buffer, 100, fpIn)!= NULL)
    {
        //First time through only, gets event name
        if(eventName[0] == '\0')
        {
            sscanf(buffer, "%[^/]", eventName);
        //After that it scans in names and corresponding data.
        } else if(i == 8) {
            sscanf(buffer, "%[^\n]", add[k].name);
            i = 0;
        } else {
            if(i < 8) {
                sscanf(buffer, "%d %f %d %d %d %d %d %d %d %d %d %d %d %d",
                &add[k].element[i], &add[k].baseValue[i], &add[k].scores[i][0], &add[k].scores[i][1],
                &add[k].scores[i][2], &add[k].scores[i][3], &add[k].scores[i][4], &add[k].scores[i][5],
                &add[k].scores[i][6], &add[k].scores[i][7], &add[k].scores[i][8], &add[k].scores[i][9],
                &add[k].scores[i][10], &add[k].scores[i][11]);
                i++;
                if(i == 8) k++;
            }
        }
    }
    (*num) = k;

    return add;
}

void sortSelect(SKATER add[], int num)
{
    //Pre: Array of structures (skaters), number of skaters
    //Post: Sorted array of structures
    int current; int walker;
    int smallestIndex;
    SKATER temp;

    for (current = 0; current < num - 1; current++)
    {
        smallestIndex = current;
        for (walker = current; walker < num; walker++)
          {     //Sorts in descending order, highest score first
                if (add[walker].finalScore > add[smallestIndex].finalScore)
                  smallestIndex = walker;
          }//for walker

        //Swap to position smallest at what is the current position
        temp = add[current];
        add[current] = add[smallestIndex];
        add[smallestIndex] = temp;

    }//for current
  return;
}

void compute(SKATER player[], int* num)
{
    //Pre: Array of structures of skaters, actual number of skaters
    //Post: Finished filling out the array of structures.
    int i, j, k;
    int high[8];
    int low[8];
    float subScore[8];

//k is incremented for each structure in the array, corresponding to each skater.
for(k = 0; k < *num; k++)
{   //i represents the number of elements for each skater.
    for(i = 0; i < 8; i++)
    {
        player[k].totalBase += player[k].baseValue[i];
    }

    for(i = 0; i < 8; i++)
    {   //j represents the number of judges.
        for(j = 0; j < 12; j++)
        {
            player[k].totalScores[i] += player[k].scores[i][j];
        }
        //function calls to find the highest and lowest score for each element
        high[i] = findHigh(player, i);
        low[i] = findLow(player, i);

        subScore[i] = (player[k].totalScores[i] - high[i] - low[i])/10;

        //Determining final score by assessing base values
        if(player[k].baseValue[i] < 3.1)
        {
            player[k].finalScore += (subScore[i]/2) + player[k].baseValue[i];
        }
        else if(player[k].baseValue[i] > 3.1)
        {
            player[k].finalScore += subScore[i] + player[k].baseValue[i];
        }
    }
}
    return;
}

int findHigh (SKATER* player, int element)
{
    //Pre: One skater passed by reference, element number
    //Post: The highest score given for the element.
    int i;
    int big = player->scores[element][0];

    //Simple search for largest score.
    for(i = 1; i < 12; i++)
    {
        if(player->scores[element][i] > big)
            big = player->scores[element][i];
    }

    return big;
}

int findLow (SKATER* player, int element)
{
    //Pre: One skater passed by reference, element number
    //Post: The lowest score given for that element.
    int i;
    int small = player->scores[element][0];
    //Simple search for lowest score.
    for(i = 1; i < 12; i++)
    {
        if(player->scores[element][i] < small)
            small = player->scores[element][i];
    }

    return small;
}

void output(SKATER* add, int num, char eventName[])
{
    //Pre: The array of structures for skaters, the actual number
    //     of skaters, the string for the event name
    //Post: None (output in file)
    FILE* fpOut;
    int k = 0;
    int i = 0;
    //Making output file
    fpOut = fopen("Lab6Output.txt", "w");
    //Title portions
    fprintf(fpOut, "Event: %s, Chief Accountant: Eric Schultz\n", eventName);
    fprintf(fpOut, "\nELEMENT     BASE VALUE   SCORES                           TOTAL BASE VALUE    TOTAL SCORE\n");
    //Loop executed to print out every structure in array
    while(k < num)
    {
        fprintf(fpOut, "\n%s\n", add[k].name);

        for(i = 0; i < 8; i++)
        {
            fprintf(fpOut, "%d\t\t%1.1f\t%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d%10.2f%15.2f\n",
                add[k].element[i], add[k].baseValue[i], add[k].scores[i][0], add[k].scores[i][1],
                add[k].scores[i][2], add[k].scores[i][3], add[k].scores[i][4], add[k].scores[i][5],
                add[k].scores[i][6], add[k].scores[i][7], add[k].scores[i][8], add[k].scores[i][9],
                add[k].scores[i][10], add[k].scores[i][11], add[k].totalBase, add[k].finalScore);
        }
        k++;
    }
    //Closing output file
    fclose(fpOut);

    return;
}
