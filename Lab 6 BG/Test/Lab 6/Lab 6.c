/*
Eric Schultz
Lab 6
eschultz@pacbell.net
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SKATERS 24

typedef struct skater
                    {
                       char name[50];
                       int element[8];
                       float baseValue[8];
                       float totalBase;
                       int scores[8][12];
                       int totalScores[8];
                       float finalScore[8];
                    }SKATER;

SKATER* getData(char eventName[], int* num);
void sortSelect(SKATER* add, int num);
void output(SKATER* add, int num, char eventName[]);
void compute(SKATER player[], int* num);
int findHigh(SKATER* player, int element);
int findLow(SKATER* player, int element);

int main (void)
{
    char eventName[30];
    //SKATER add[MAX_SKATERS];
    SKATER* add;
    int num = 0;

    add = getData(eventName, &num);

    compute(add, &num);

    //sortSelect(add, num);

    output(add, num, eventName);

    return 0;
}

SKATER* getData(char eventName[], int* num)
{
    FILE* fpIn;
    char buffer[100];
    int i = 0;
    int j = 0;
    int k = 0;
    SKATER* add;
    int tempElement;
    float tempBase;

    add = (SKATER*)calloc(MAX_SKATERS, sizeof(SKATER));

    if(!(fpIn = fopen("OlympicMensShort.txt", "r")))
        {
         printf("FILE NOT FOUND\n");
         exit(100);
        }

    /*while(fgets(buffer, 100, fpIn)!= NULL)
    {
        if(j == 0)
        {
            sscanf(buffer, "%[^/]", eventName);
            printf("Event Name: %s\n", eventName);
            j++;
        }
        else if((j-1)%9 == 0)
        {
            sscanf(buffer, "%[^\n]", add[k].name);
            printf("name: %s\n", add[k].name);
            j++;
            k++;
        }
        else
        {
            sscanf(buffer, "%d %f %d %d %d %d %d %d %d %d %d %d %d %d",
                &(add[k].element[i]), &(add[k].baseValue[i]), &(add[k].scores[i][0]), &(add[k].scores[i][1]),
                &(add[k].scores[i][2]), &(add[k].scores[i][3]), &(add[k].scores[i][4]), &(add[k].scores[i][5]),
                &(add[k].scores[i][6]), &(add[k].scores[i][7]), &(add[k].scores[i][8]), &(add[k].scores[i][9]),
                &(add[k].scores[i][10]), &(add[k].scores[i][11]));

            printf("%d %1.1f %d %d %d %d %d %d %d %d %d %d %d %d\n",
                add[k].element[i], add[k].baseValue[i], add[k].scores[i][0], add[k].scores[i][1],
                add[k].scores[i][2], add[k].scores[i][3], add[k].scores[i][4], add[k].scores[i][5],
                add[k].scores[i][6], add[k].scores[i][7], add[k].scores[i][8], add[k].scores[i][9],
                add[k].scores[i][10], add[k].scores[i][11]);
            i++;
            j++;
        }
    }*/

    while(fgets(buffer, 100, fpIn)!= NULL)
    {
        if(j == 0)
        {
            sscanf(buffer, "%[^/]", eventName);
            printf("Event Name: %s\n", eventName);
            j++;
        }
        else if((j-1)%9 == 0)
        {
            sscanf(buffer, "%[^\n]", add[k].name);
            printf("name: %s\n", add[k].name);
            j++;
            k++;
        }
        else
        {
            sscanf(buffer, "%d %f",
                &tempElement, &tempBase);
        }
    }

    printf("\n\n");

    printf("from temps: %d %3.1f\n", tempElement, tempBase);

    add[0].element[0] = tempElement;
    add[0].baseValue[0] = tempBase;

    printf("from struct: %d %3.1f\n\n", add[0].element[0], add[0].baseValue[0]);

    for(k = 0; k < 5; k++)
    {
        for(i = 0; i < 8; i++)
        {
            printf("** %d %1.1f %d %d %d %d %d %d %d %d %d %d %d %d\n",
                add[k].element[i], add[k].baseValue[i], add[k].scores[i][0], add[k].scores[i][1],
                add[k].scores[i][2], add[k].scores[i][3], add[k].scores[i][4], add[k].scores[i][5],
                add[k].scores[i][6], add[k].scores[i][7], add[k].scores[i][8], add[k].scores[i][9],
                add[k].scores[i][10], add[k].scores[i][11]);
        }
    }

    printf("\n\n");


    for(i = 0; i < 8; i++)
    {
        printf("Base Value: %3.1f\n", add[1].baseValue[i]);
    }

    (*num) = k;
    printf("\nnum: %d\n\n", *num);

    return add;
}

/*void sortSelect(SKATER* add, int num)
{
    int current; int walker;
    int smallestIndex;
    SKATER temp;
    int j, k;

    for (current = 0; current < num - 1; current++)
    {
        smallestIndex = current;
    for(k = 0; k < num; k++)
      {
        for(j = 0; j < 12; j++)
        {
        for (walker = current; walker < num; walker ++)
          {
                if (add[k].scores[walker][j] < add[k].scores[smallestIndex][j])
                  smallestIndex = walker;
          }//for walker

        //Swap to position smallest at what is the current position
        temp.scores[current][j] = add[k].scores[current][j];
        add[k].scores[current][j] = add[k].scores[smallestIndex][j];
        add[k].scores[smallestIndex][j] = temp.scores[smallestIndex][j];
        }//for j
      }//for k
    }//for current
  return;
}*/

void compute(SKATER player[], int* num)
{
    int i, j, k;
    int high[8];
    int low[8];
    //float subScore[8];


for(k = 1; k < (*num + 1); k++)
{
    for(i = 0; i < 8; i++)
    {
        player[k].totalBase += player[k].baseValue[i];
        printf("Base Value: %3.1f\n", player[k].baseValue[i]);
    }

    printf("\n\nTotal of base values: %4.1f\n\n", player[k].totalBase);
}

    for(i = 0; i < 8; i++)
    {
        for(j = 0; j < 12; j++)
        {
            player[k].totalScores[i] += player[k].scores[i][j];
        }

        high[i] = findHigh(player, i);
        low[i] = findLow(player, i);
        player[k].finalScore[i] = (player[k].totalScores[i] - high[i] - low[i])/10;

        if(player[k].baseValue[i] < 3.1)
        {
            player[k].finalScore[i] += (float)(player[k].totalScores[i]/2) + player[k].baseValue[i];
        }
        else if(player[k].baseValue[i] > 3.1)
        {
            player[k].finalScore[i] += player[k].totalScores[i] + player[k].baseValue[i];
        }
    }

    return;
}

int findHigh (SKATER* player, int element)
{
    int i;
    int big = player->scores[element][0];


    for(i = 1; i < 12; i++)
    {
        if(player->scores[element][i] > big)
            big = player->scores[element][i];
    }

    return big;
}

int findLow (SKATER* player, int element)
{
    int i;
    int small = player->scores[element][0];

    for(i = 1; i < 12; i++)
    {
        if(player->scores[element][i] < small)
            small = player->scores[element][i];
    }

    return small;
}

void output(SKATER* add, int num, char eventName[])
{
    FILE* fpOut;
    int k = 0;
    int i = 0;

    fpOut = fopen("Lab6Output.txt", "w");

    fprintf(fpOut, "Event: %s, Chief Accountant: Eric Schultz\n", eventName);
    fprintf(fpOut, "\n\nSKATER");
    fprintf(fpOut, "\nELEMENT   BASE VALUE    SCORES    TOTAL BASE VALUE    TOTAL SCORE\n\n");

    while(k<=num)
    {
        if((i)%9 == 0)
        {
            fprintf(fpOut, "name: %s\n", add[k].name);
            i++;
            k++;
        }
        else
        {
            fprintf(fpOut, "%d %1.1f %d %d %d %d %d %d %d %d %d %d %d %d %3.1f %3.2f\n",
                add[k].element[i], add[k].baseValue[i], add[k].scores[i][0], add[k].scores[i][1],
                add[k].scores[i][2], add[k].scores[i][3], add[k].scores[i][4], add[k].scores[i][5],
                add[k].scores[i][6], add[k].scores[i][7], add[k].scores[i][8], add[k].scores[i][9],
                add[k].scores[i][10], add[k].scores[i][11], add[k].totalBase, add[k].finalScore);
            i++;
        }
    }

    fclose(fpOut);

    return;
}
