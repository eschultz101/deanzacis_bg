/*Write a function that accepts a string (a pointer to a character)
and deletes the last character by moving the null character one
position to the left.

Also write a main function that inputs your family name, calls
this function, and then ourputs your family name after the call
(it should be missing the last letter at this point). Copy your
code and the output (as a comment at the bottom of the code) as
run by entering your name.*/

#include <stdio.h>
#include <string.h>

int strDelete(char* famName);

int main (void)
{
    char famName[20];
    int length;
    int i;

    printf("Type family name here: ");
    scanf("%s", famName);

    length = strDelete(famName);

    for(i = 0; i < length; i++)
        printf("%c", famName[i]);

    return 0;
}

int strDelete(char* famName)
{
    int i = 0;
    int len = 0;

    len = strlen(famName);

    len = len - 1;

    return len;
}
