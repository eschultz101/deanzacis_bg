/*Eric Schultz
Lab 4
eschultz@pacbell.net*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float* getData(FILE*, int*);
float** point(float*, int);
void calc(float**, int, float*, float*);
void selectSort(float**, int);
void output(FILE*, float**, int, float, float);

int main (void)
{
    //Pre: None
    //Post: Declared files in use and variables for the array
    //of readings, a pointer to the array of readings, the max number of readings,
    //the mean and standard deviation.

    //Variable declarations
    FILE* fpIn; FILE* fpOut;
    float* readings;
    float** ptrRead;
    int num;
    float mean;
    float stdDev;

    //Fucntion declarations
    readings = getData(fpIn, &num);
        printf("GOT HERE 1\n");
    ptrRead = point(readings, num);
        printf("GOT HERE 2\n");
    calc(ptrRead, num, &mean, &stdDev);
        printf("GOT HERE 3\n");

    output(fpOut, ptrRead, num, mean, stdDev);
        printf("GOT HERE 4\n");

    return 0;
}

float* getData(FILE* fpIn, int* num)
{
    //Pre: Input file, array of readings, pointer to array of readings, number of readings.
    //Post: Array of readings and number of readings now scanned to their respective variables.

    int i = 0;
    float* readings;

    if((fpIn = fopen("readings.txt", "r")) == NULL)
    {
        printf("No such file");
        exit(100);
    }

    //Scanning in data from input file.
    fscanf(fpIn, "%d", num);

    readings = (float*)calloc(*num, sizeof(float));

    while(fscanf(fpIn, "%f", (readings + i)) != EOF)
    {
        i++;
    }

    return readings;
}

float** point(float* readings, int num)
{
    //Pre: Recieves the array of readings and the address of the pointer to that array,
    //as well as the number of readings in the array.
    //Post: The pointer to the array of readings is now actually pointing to the array of readings.
    int i;
    float** ptrRead;

    ptrRead = (float**)calloc(num, sizeof(float*));

    //ptrRead is the pointer pointing to the first element of newly allocated memory.
    for(i = 0; i < num; i++)
    {
        *(ptrRead + i) = readings + i;
        printf("point %f \n", **(ptrRead + i));
    }

    return ptrRead;
}

void calc(float** ptrRead, int num, float* mean, float* stdDev)
{
    //Pre: Pointer to the array of readings, the number of readings,
    //the mean and standard deviation variables.
    //Post: Mean and standard deviation now have meaningful values.

    int i;
    float sumMeans = 0;
    float sum = 0;

    //Calculating mean of data

    printf(" GOT HERE calc\n");

    for(i = 0; i < num; i++)
    {
        printf("calc %f \n", **(ptrRead + i));
        sum += **(ptrRead + i);
    }

    *mean = sum/num;

    //Calculations for standard deviation

    for(i = 0; i < num; i++)
        sumMeans += (((**(ptrRead + i) - *mean))*((**(ptrRead + i) - *mean)));

    *stdDev = sqrt((1/((float)num))*sumMeans);

    return;
}

void selectSort(float** ptrRead, int num)
{
    //Pre: Unsorted array of readings (through pointer to that array) and
    //the number of readings.
    //Post: Sorted array of readings.

    int current; int walker; int j; int k; int i;
    int smallestIndex;
    float temp;

    //sorts in ascending order
    for (current = 0; current < num - 1; current++)
    {
        smallestIndex = current;
        for (walker = current; walker < num; walker++)
          {
                if (**(ptrRead + walker) < **(ptrRead + smallestIndex))
                    smallestIndex = walker;
          }//for walker

        temp = **(ptrRead + current);
        **(ptrRead + current) = **(ptrRead + smallestIndex);
        **(ptrRead + smallestIndex) = temp;

    }//for current

    return;
}

void output(FILE* fpOut, float** ptrRead, int num, float mean, float stdDev)
{
    //Pre: Output file, pointer to array of readings, number of readings,
    //mean and standard deviation.
    //Post: Output tables outputted.

    int i;
    //Create output file.
    fpOut = fopen("lab 4 output.txt", "w");

    fprintf(fpOut, "Eric Schultz\nLab 4\neschultz@pacbell.net\n\n");

    //Table of original values.
    fprintf(fpOut, "Original values: \n\n");
    for(i = 0; i < num; i++)
    {
        fprintf(fpOut, "%5.1f ", **(ptrRead + i));
        if((i+1)%8 == 0)
            fprintf(fpOut, "\n");
    }

    //Call to sort function to get sorted values.
    selectSort(ptrRead, num);

    //Table of sorted values.
    fprintf(fpOut, "\n\n");
    fprintf(fpOut, "Sorted values: \n\n");
    for(i = 0; i < num; i++)
    {
        fprintf(fpOut, "%5.1f ", **(ptrRead + i));
        if(**(ptrRead + i) > mean + (1.5f*stdDev) || **(ptrRead + i) < mean - (1.5f*stdDev))
            fprintf(fpOut, "Out of range");
        if((i+1)%1 == 0)
            fprintf(fpOut, "\n");

    }

    return;
}

