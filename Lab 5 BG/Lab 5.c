/*
Eric Schultz
Lab 5
eschultz@pacbell.net
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 100

int getData(char* words[]);
void clean(char* words[], int num);
void checkDuplicates(char* words[], int wordCount[], int num);
void selectSort(char* words[], int wordCount[], int num);

int main(void)
{
    //Pre: None
    //Post: Array with words, parallel array counting
    //number of each word, total number of words
char* words[MAX];
int wordCount[MAX] = {0};
int num; int i;
//Output your name
    printf("Eric Schultz\nLab 5\neschultz@pacbell.net\n\n");

//Call function to input words here
    num = getData(words);
//Clean words
    clean(words, num);
//Checking for duplicates of each word and adding one if there is one
    checkDuplicates(words, wordCount, num);
//Sorting words in alphabetical order
    selectSort(words, wordCount, num);
//Outputting words and number of each
    for(i = 0; i < num; i++)
    {
        if(wordCount[i] != -1)
            printf("%15s\t%d\n", words[i], wordCount[i]);
    }

    return 0;
}

int getData(char* words[])
{
    //Pre: Array of words (with no words in it)
    //Post: Array of words (with words in it)
    FILE* fpIn;
    char temp[50];
    int num = 0;
    //Checking for file existence
    if((fpIn=fopen("words.txt","r"))== NULL)
    {
        printf("No such file\n");
        system("pause");
        exit(100);
    }
    //Scanning in each word as a string and allocating memory for each
    while((num < MAX) && (fscanf(fpIn, "%s", temp) != EOF))
    {
        words[num] = (char*)calloc(strlen(temp)+1, sizeof(char));
        //Copying amount of space from the temp location to the words array
        strcpy(words[num], temp);
        num++;
    }
    return(num);
}

void clean(char* words[], int num)
{
    //Pre: Array of words, number of words
    //Post: Array of words (cleaned)
    int i, j, k;
    char temp[50];

    //Loop to check for nonalpha characters and "remove" them
    for(i = 0; i < num; i++)
    {
        k = 0;
        for(j = 0; *(words[i] + j) != '\0'; j++)
        {
            //checking for alphas
            if(isalpha(*(words[i] + j)))
            {
                //temp[k] holds each letter (alphas only)
                temp[k++] = tolower(*(words[i] + j));
            }
        }
        //adding the end of string character at the end
        temp[k] = '\0';
        //copying each word (now only alphanumeric) into the words array
        strcpy(words[i], temp);
    }

    return;
}

void checkDuplicates(char* words[], int wordCount[], int num)
{
    //Pre: Array of cleaned words, parallel array with the number
    //of each word,total number of words
    //Post: Parallel array of the number of each words now has meaningful values
    int i, j;

    for(i = 0; i < num; i++)
    {
        if(wordCount[i] == 0) //if the word hasn't been encountered, then...
        {
            wordCount[i] = 1; //set it to one because it found one
            for(j = i + 1; j < num; j++)
            {
                if(strcmp(words[i], words[j]) == 0) //if the two words are the same, then...
                {
                    wordCount[i] = wordCount[i] + 1; //add one to the counter.

                    wordCount[j] = -1; //flags all other words encountered that
                                       //are the same word as one already found
                }
            }
        }
    }

    return;
}

void selectSort(char* words[], int wordCount[], int num)
{
    //Pre: Array of words, the number of each word, total number of words
    //post: Sorted arrays of words and number of each word
    int i, j, min;
    char* tempc;
    int temp;

    for (i = 0; i < num; i++)
    {
        min = i;
        for (j = i+1; j < num; j++)
        {
            if (strcmp(words[j],words[min]) < 0)
            {
                min = j;
            }
        }
        //Swapping words into alphabetical order
        tempc = words[min];
        words[min] = words[i];
        words[i] = tempc;
        //Swapping the word counts for each word
        temp = wordCount[min];
        wordCount[min] = wordCount[i];
        wordCount[i] = temp;
    }
}

