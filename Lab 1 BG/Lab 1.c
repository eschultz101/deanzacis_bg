/*
Eric Schultz
Lab 1
eschultz@pacbell.net
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

void sortSelect(int studentPin[], int numStudents, int numCorrect[]);
void output(int studentPin[], int numQuestions, int numCorrect[], int numStudents);

int main(void)
{
    //Pre: None
    //Post: Student pins, the number of students, the number of questions
    //each got correct, and the number of total questions are being sent via pass by value or reference.
    int studentPin[50];
    char key[100];
    int numQuestions;
    int numCorrect[50];
    char answer;
    int numStudents;
    float percentCorrect;
    FILE* fpIn;
    int i;

    //Checking for the input file's existence.
    if((fpIn=fopen("lab1.txt","r"))== NULL)
        {
          printf("No such file\n"); system("pause");
          exit(100);
        }
        //Scanning in all necessary information from the .txt file
    fscanf(fpIn, "%d", &numQuestions);
    printf("Total number of questions: %d \n", numQuestions);
    printf("Key: ");
    for(i= 0; i<=numQuestions; i++)
    {
        fscanf(fpIn, "%c", &key[i]);
    }
    for(i = 0; i<=numQuestions; i++)
    printf("%c", key[i]);
    printf("\n\n");
    numStudents = 0;
    int j;
    while(fscanf(fpIn, "%d", &studentPin[numStudents]) != EOF)
    {
        numCorrect[numStudents] = 0;
        //Scan one extra to account for newline char at end
        for(j = 0; j<numQuestions+1; j++)
        {
            fscanf(fpIn, "%c", &answer);

            if(answer == key[j])
            {
                numCorrect[numStudents]++;
            }
        }
        numStudents++;
    }
    //calling subfunctions to sort student pins and output the table.
    sortSelect(studentPin, numStudents, numCorrect);

    output(studentPin, numQuestions, numCorrect, numStudents);

    return;
}

void sortSelect(int studentPin[], int numStudents, int numCorrect[])
{
    //Pre: Student pins, number of students, and the number of questions correctly answered coming in.
    //Post: Student pins and correct answers.
    int current; int walker;
    int smallestIndex;
    float tempf;
    int temp;

    //sorts in ascending order
    for (current = 0; current < numStudents - 1; current++)
    {
        smallestIndex = current;
        for (walker = current; walker < numStudents; walker++)
          {
                if (studentPin[walker] < studentPin[smallestIndex])
                  smallestIndex = walker;
          }//for walker

        //Swapping student pins
        temp = studentPin[current];
        studentPin[current] = studentPin[smallestIndex];
        studentPin[smallestIndex] = temp;

        temp = numCorrect[current];
        numCorrect[current] = numCorrect[smallestIndex];
        numCorrect[smallestIndex] = temp;

    }//for current
    return;
}

void output(int studentPin[], int numQuestions, int numCorrect[], int numStudents)
{
    //Pre: Student pins, number of total questions, number of correct questions,
    //and number of students passed in.
    //Post: None
    int i;
    float percentCorrect;
    printf("STUDENT ID\tNUMBER CORRECT\tPERCENTAGE CORRECT\n");
    for(i= 0; i<numStudents; i++)
    {
        percentCorrect = (((float)numCorrect[i])/numQuestions)*100;
        printf("%d\t\t%d\t\t%%%.f\n", studentPin[i], numCorrect[i], percentCorrect);
    }
    return;
}
