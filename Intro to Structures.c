#include<stdio.h>
#include<stdlib.h>
#include <string.h>

#define LENGTH 20
#define MAX 40

typedef struct student{
                      char name[LENGTH];
                      char grade;
  float points;
}STUDENT;

void input(STUDENT*);
int inFile(STUDENT []);
void printRecord(STUDENT);
void sortSelect(STUDENT arr[], int num);

int main(void)
{
STUDENT instructor;
STUDENT cis15BG[MAX];
int number;  int i;
//Get Instructor's info
//input(&instructor);

//printRecord(instructor);

//Get info for entire class
number = inFile(cis15BG);
sortSelect(cis15BG, number);
for (i = 0; i < number; i++)
  printRecord(cis15BG[i]);


system("pause");
return 0;
}
void sortSelect(STUDENT arr[], int num)
{
    int current; int walker;
    int smallestIndex;
    STUDENT temp;

    for (current = 0; current < num - 1; current++)
    {
        smallestIndex = current;
        for (walker = current; walker < num; walker ++)
          {
                if (strcmp(arr[walker].name, arr[smallestIndex].name)<0)
                  smallestIndex = walker;
          }//for walker

        //Swap to position smallest at what is the current position
        temp = arr[current];
        arr[current] = arr[smallestIndex];
        arr[smallestIndex] = temp;
    }//for current
  return;
}
int inFile(STUDENT cis[])
{
    int number = 0; FILE* fpIn; char buffer[256];
if ((fpIn = fopen("finalGrades.txt","r"))==NULL)
            {printf("File opening error\n"); system("pause");exit(100);}

    while (number < MAX && fgets(buffer, sizeof(buffer) - 1, fpIn))
      { puts(buffer);
      sscanf(buffer, "%c%f%*c%19[^\n]", &cis[number].grade,
                          &cis[number].points,
                          cis[number].name);
      //printf("\n%c\n", cis[number].grade);
      number++;
      }
    return number;
}

void input(STUDENT* newStudent)
{
system("pause");
printf("Enter student's name\n");
fscanf(stdin,"%[^\n]",newStudent->name);

printf("Enter student's Points\n");
fscanf(stdin,"%f",&newStudent->points);

printf("Enter student's Grade\n");
fscanf(stdin," %c",&(*newStudent).grade);

    printRecord(*newStudent);
return;
}
///////////////
void printRecord(STUDENT any)
{
    printf("%s\t",any.name);
    printf("%.1f\t",any.points);
    printf("%c\n",any.grade);

    return;
    }//printRecord
