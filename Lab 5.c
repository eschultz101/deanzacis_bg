/*
Eric Schultz
Lab 5
eschultz@pacbell.net
*/

#include <stdio.h>
#include <math.h>

#define TAX_RATE 0.0125 //x10 = percent
#define INSURANCE_COST 550.00 //$ a year
#define UTILITIES 300.00 //$ a month

void get_data(double*, float*, float*);
void calculations(double* monthlyMortgage, float years,
                  double* a, double downP, double* down,
                  double* utilitiesCost, double* insuranceCost,
                  float iRate, double* propertyTaxes,
                  double price, double* grandTotal);
double monthly_mortgage(float, double*, float);
void output(double price, double down, double amount, float interestRate,
            int years, double monthlyMortgage, double utilitiesCost,
            double propertyTaxes, double grandTotal, double insuranceCost);

int main (void)
{
    double amount;
    double monthlyMortgage;
    double price;
    float interestRate;
    float years;
    float const downPercent = 0.2;
    double downPayment;
    double utilitiesCost;
    double insuranceCost;
    double propertyTaxes;
    double grandTotal;

    get_data(&price, &interestRate, &years);
    calculations(&monthlyMortgage, years, &amount, downPercent,
                 &downPayment, &utilitiesCost, &insuranceCost, interestRate,
                 &propertyTaxes, price, &grandTotal);
    output(price, downPayment, amount, interestRate,
             years, monthlyMortgage, utilitiesCost,
             propertyTaxes, grandTotal, insuranceCost);

    return 0;
}

void get_data(double* p, float* iRate, float* y)
{
    printf("Enter selling price: ");
    scanf("%lf", p);
    printf("\nEnter rate of interest: ");
    scanf("%f", iRate);
    printf("\nEnter number of years of loan: ");
    scanf("%f", y);

    return;
}

void calculations(double* monthlyMortgage, float years,
                  double* a, double downP, double* down,
                  double* utilitiesCost, double* insuranceCost,
                  float iRate, double* propertyTaxes,
                  double price, double* grandTotal)
{
    //Pre: selling price, interest rate, years of loan
    //Post: various costs, amount of loan, monthly mortgage, total of monthly costs

    //utilities, insurance
    *utilitiesCost = UTILITIES;
    *insuranceCost = INSURANCE_COST/12;
    *propertyTaxes = price*TAX_RATE/12;

    *down = downP*price;

    //amount of loan
    *a = price*0.8;

    //calculate mortgage
    *monthlyMortgage = monthly_mortgage(years, a, iRate);


    //total
    *grandTotal = *monthlyMortgage+*utilitiesCost+*propertyTaxes+*insuranceCost;

    return;
}

double monthly_mortgage(float y, double* a, float i)
{   //Pre: years, amount, interest rate
    //Post: monthly mortgage cost
    double mm;
    int n;

    n = y*12;
    i/=1200; //interest turned into percent from int and months from years
    mm = (*a*i*(pow(1+i, n)))/(pow(1+i, n)-1);

    return mm;
}

void output(double price, double down, double amount, float interestRate,
            int years, double monthlyMortgage, double utilitiesCost,
            double propertyTaxes, double grandTotal, double insuranceCost)
{   //Pre: Everything that needs to be in the output
    //Post: nothing

    //General info display
    printf("\n\nMONTHLY COST OF HOUSE\n\n");
    printf("SELLING PRICE            $%9.2lf", price);
    printf("\nDOWN PAYMENT              %9.2lf", down);
    printf("\nAMOUNT OF LOAN            %9.2lf", amount);
    printf("\nINTEREST RATE                  %3.1f%%", interestRate);
    printf("\nTAX RATE                       %3.1f%%", TAX_RATE*100);
    printf("\nDURATION OF LOAN (YEARS)         %d\n", years);
    //monthly costs
    printf("\nMONTHLY PAYMENT");
    printf("\n\tMORTGAGE           %8.2lf", monthlyMortgage);
    printf("\n\tUTILITIES          %8.2lf", utilitiesCost);
    printf("\n\tPROPERTY TAXES     %8.2lf", propertyTaxes);
    printf("\n\tINSURANCE COST     %8.2lf", insuranceCost);
    printf("\n\t\t\t  --------");
    printf("\n\t\t\t $ %8.2lf", grandTotal);

    printf("\n\nEric Schultz\neschultz@pacbell.net\nLab 5\n\n");

    return;
}

