/*Eric Schultz
Lab 2
eschultz@pacbell.net
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define NUM_DIVERS 50
#define NUM_JUDGES 5

int getdata(char names[][20], float difficulty[], float scores[][NUM_JUDGES]);
void bubbleSort(float scores[][NUM_JUDGES], int num);
void calc(float scores[][NUM_JUDGES], float difficulty[], float totalScore[], int num);
void sortSelect(char names[][20], float scores[][NUM_JUDGES], float totalScore[], int num);
void output(char names[][20], float scores[][NUM_JUDGES], float totalScore[], int num);

int main (void)
{
    //Pre: None
    //Post: Arrays for the names of the divers, each of their difficulties,
    //and each of their scores from the 5 judges.

    int i; int j; int num;
    char names[NUM_DIVERS][20]; //number of divers, number of characters in their names
    float difficulty[NUM_DIVERS]; //difficulties in a one dim array
    float scores[NUM_DIVERS][NUM_JUDGES]; //two dim array for the scores

    num = getdata(names, difficulty, scores);

    //declare the total score array AFTER the actual number of divers has been determined.
    float totalScore[num];

    //Subroutines for sorting, calculating, sorting again, and outputting.
    bubbleSort(scores, num);

    calc(scores, difficulty, totalScore, num);

    sortSelect(names, scores, totalScore, num);

    output(names, scores, totalScore, num);

    return 0;
}

int getdata(char names[][20], float difficulty[], float scores[][NUM_JUDGES])
{
    //Pre: Arrays for names, difficulties, and scores.
    //Post: These same arrays with meaningful values, and the actual number of divers.
    int num;
    int n;
    FILE* fpIn;
    float diff, s0, s1, s2, s3, s4;
    char name[19];
    //Checking for file existence.
    if((fpIn=fopen("DIV1.txt","r")) == NULL)
    {
        printf("No such file\n"); system("pause");
        exit(100);
    }
    else
    {
        //Scanning from input file and assigning values.
        num = 0;
        while(fscanf(fpIn, "%18c %f %f %f %f %f %f", &name, &diff, &s0, &s1, &s2, &s3, &s4) != EOF)
        {
            name[18] = '\0';

            for(n = 0; n < 19; n++) names[num][n] = name[n];
            difficulty[num] = diff;
            scores[num][0] = s0;
            scores[num][1] = s1;
            scores[num][2] = s2;
            scores[num][3] = s3;
            scores[num][4] = s4;

            num++;
        }
        fclose(fpIn);
        return num;
    }
}

void bubbleSort(float scores[][NUM_JUDGES], int num)
{
  //Pre: Array of scores and actual number of divers.
  //Post: Array of scores sorted.
  int i, j, k;
  float temp;
  int sorted;

for(k = 0; k < num; k++)
{
  for (i = num; i >= 1; i--)
  {
    for (j = 1; j <= i; j++)
    {
      if(scores[k][j-1] > scores[k][j])
      {
        //Swapping scores.
        temp = scores[k][j-1];
        scores[k][j-1] = scores[k][j];
        scores[k][j] = temp;
      }
    }
  }
}
 return;
}

void calc(float scores[][NUM_JUDGES], float difficulty[], float totalScore[], int num)
{
    //Pre: Array of scores, difficulties, total scores, and the actual number of divers.
    //Post: Array of scores, difficulties, and total scores.
    int i; float avg;
    /*calculate the average score based on the three
    middle scores, then multiply by the difficulty.*/
    for(i = 0; i < num; i++)
        {
            avg = (scores[i][1]+scores[i][2]+scores[i][3])/3;
            //Assigning each diver their total score.
            totalScore[i] = avg*difficulty[i];
        }

    return;
}

void sortSelect(char names[][20], float scores[][NUM_JUDGES], float totalScore[], int num)
{
    //Pre: Array of names, total scores, and the actual number of divers.
    //Post: Array of names and total scores, sorted.
    int current; int walker; int j; int k;
    int smallestIndex;
    float temp;
    char tempc;

    //sorts in ascending order
    for (current = 0; current < num - 1; current++)
    {
        smallestIndex = current;
        for (walker = current; walker < num; walker++)
          {
                if (totalScore[walker] < totalScore[smallestIndex])
                  smallestIndex = walker;
          }//for walker

        //Swapping final scores
        temp = totalScore[current];
        totalScore[current] = totalScore[smallestIndex];
        totalScore[smallestIndex] = temp;

        //Swapping middle three scores
        for(k = 0; k < NUM_JUDGES; k++)
        {
            temp = scores[current][k];
            scores[current][k] = scores[smallestIndex][k];
            scores[smallestIndex][k] = temp;
        }//for k

        for(j = 0; j < 20; j++)
        {
            //swapping names
            tempc = names[current][j];
            names[current][j] = names[smallestIndex][j];
            names[smallestIndex][j] = tempc;
        }//for j

    }//for current
    return;
}

void output(char names[][20], float scores[][NUM_JUDGES], float totalScore[], int num)
{
    //Pre: Arrays for names, scores, and total scores and the actual number of divers.
    //Post: Nothing, except for printing a table to output file.
    int i, j;
    FILE* fpOut;

    if((fpOut=fopen("output.txt","w"))== NULL)
        {
          printf("Can't open output file\n"); system("pause");
          exit(100);
        }

    fprintf(fpOut, "Eric Schultz\nLab 2\neschultz@pacbell.net\n\n");

    //table with names, scores (low to high), total score.
    fprintf(fpOut, "NAME\t\t\tSCORES\t\tTOTAL SCORE\n");

    for(i = 0; i < num; i++)
    {
        for(j = 0; j < 19; j++)
        {
            fprintf(fpOut, "%c", names[i][j]);
        }
        fprintf(fpOut, "\t");
        fprintf(fpOut, "%2.1f %2.1f %2.1f\t%2.1f\n", scores[i][1],
                            scores[i][2], scores[i][3], totalScore[i]);
    }
    //Closing files
    fclose(fpOut);

    return;
}
