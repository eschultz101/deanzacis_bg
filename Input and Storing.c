#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 100

void getData(char**, int*);

int main(void)
{
    char* words[MAX];
    int num; int i;
    //Output your name
    printf("Eric Schultz\n\n");
    
    //Call function to input words here
    getData(words, &num);
    
    //Output words one per line
    for(i = 0; i < num; i++)
    printf("%s\n", words[i]);
    system("pause");
    return 0;
}

void getData(char* words[], int* num)
{
    FILE* fpIn;
    char temp[30];
    temp[29] = '\0';
    *num = 0;

    if((fpIn=fopen("store.txt","r"))== NULL)
    {
        printf("No such file\n");
        system("pause");
        exit(100);
    }

    while(*num < MAX && (fscanf(fpIn, "%s", &temp) != EOF))
    {
        words[*num] = (char*)calloc(strlen(temp), sizeof(char));
        strcpy(words[*num], temp);
        (*num)++;
    }

    return;
}


